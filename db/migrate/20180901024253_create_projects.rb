class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :title
      t.string :slug
      t.text :body
      t.string :thumbnail
      t.string :download
      t.string :downloadDescription
      t.string :description
      t.string :tags
      t.boolean :published

      t.timestamps
    end
  end
end
