class ProjectsController < ApplicationController
	#ADMIN_USERNAME and PASSWORD are set in the credentials file by running "EDITOR=nano rails credentials:edit"
	ADMIN = { Rails.application.credentials.ADMIN_USERNAME => Rails.application.credentials.ADMIN_PASSWORD }
	before_action :authenticate, except: [:index, :show, :downloads]

  def index
		  @projects = Project.order(:created_at => :asc).paginate(page: params[:page], per_page: 10)
  end

  def downloads
		  @projectDownloads = Project.order(:created_at => :asc).paginate(page: params[:page], per_page: 15)
  end

  def masterIndex
		  @projects = Project.order(:created_at => :asc).paginate(page: params[:page], per_page: 20)
  end

  def show
	  @project = Project.find_by(slug: params[:id])
  end

  def new
	  @project = Project.new
  end

  def create
	  @project = Project.new(project_params)
	  if @project.valid?
		  @project.save
	  end
	  redirect_to @project
  end

  def edit
	  @project = Project.find_by(slug: params[:id])
  end

  def update
	  @project = Project.find_by(slug: params[:id])
	  @project.update(project_params)
	  redirect_to @project
  end

  def destroy
	  @project = Project.find_by(slug: params[:id])
	  @project.destroy
  end

  private
  def authenticate
		  authenticate_or_request_with_http_digest do |username|
				  ADMIN[username]
		  end
  end

  def project_params
	  params.require(:project).permit(:title, :body, :thumbnail, :published, :download, :downloadDescription, :description, :tags)
  end
end
