class Project < ApplicationRecord
	before_validation :sanitize, :slugify

	mount_uploader :thumbnail, ThumbnailUploader
	mount_uploader :download, DownloadUploader

	def slugify
		self.slug = self.title.downcase.gsub(" ", "-")
	end

	def sanitize
		self.title = self.title.strip
	end
	
	def to_param
			slug
	end
end
