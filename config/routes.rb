Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  root 'projects#index'

  get '/home' => 'static_pages#home'
  get '/about' => 'static_pages#about'

  get '/downloads' => 'projects#downloads'
  get '/masterindex' => 'projects#masterIndex'

  resources :projects
end
