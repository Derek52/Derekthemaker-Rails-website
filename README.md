This is a relatively simple blogging webapp built in Ruby on Rails. 

This app uses Ruby 2.6.0 and Rails 5.2.

At the moment, I'm using Bulma(a nice CSS framework) for most of the frontend. I'm still configuring the frontend however.

This is simply a site for me to post projects I build, and has 1 single admin user.

The ability for other viewers of the blog to comment, is currently a work in progress.

The app is pretty powerful for the admin though, you can upload files for download, and it has a rich text editor(CkEditor) for nice formatting of blog posts.

This site uses Postgresql, but could be pretty easily modified to use whatever you wanted.
